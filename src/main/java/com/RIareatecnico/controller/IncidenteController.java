package com.RIareatecnico.controller;

import com.RIareatecnico.service.IncidenteService;
import com.RIcommons.dto.IncidenteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/incidentes")
public class IncidenteController
{
    @Autowired
    IncidenteService incidenteService;

    @GetMapping("/byTecnico/{id_tecnico}")
    public ResponseEntity<List<IncidenteDTO>> getIncidentesByTecnico(@PathVariable("id_tecnico") int id_tecnico){
        return new ResponseEntity<>(incidenteService.obtenerIncidentesPorTecnico(id_tecnico), HttpStatus.OK);
    }

    @PutMapping("/modify/{id_incidente}")
    public ResponseEntity<String> modifyIncidente(@PathVariable("id_incidente") int id_incidente,
                                                  @RequestBody IncidenteDTO incidenteDTO) throws Exception {
        incidenteService.modificarIncidente(incidenteDTO);
        return ResponseEntity.ok("El incidente ha sido modificado con exito");
    }

}
