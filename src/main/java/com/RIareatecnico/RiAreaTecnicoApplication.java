package com.RIareatecnico;

import jakarta.persistence.Entity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EntityScan(basePackages = "com.RIcommons.entity")
@ComponentScan(basePackages = "com.RIcommons.assembler")
public class RiAreaTecnicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiAreaTecnicoApplication.class, args);
	}

}
